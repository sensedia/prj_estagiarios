# Desafio 001 (v.2) - OPB Delivery

- Criar uma aplicação REST (NODE ou JAVA ou ....).
- Criar endpoint que consome até 5 consentimentos da fila de consentimento(SQS) e grava os dados no banco de dados e retorna a lista de consentIds gravado no formato JSON. 
- Criar endpoint que retorna uma lista de consentimenos obtidas no banco de dados no formato JSON.
- Criar endpoint que recebe um "consentId", consulta os logs de consentimentos no banco de dados e retorna os dados no formato JSON.
- Criar endpoint que recebe um "consentId", consulta os logs de consentimentos no banco de dados e retorna os dados no formato JWT.

Obs.1: - As credenciais de acesso a a fila AWS-SQS foram passadas por email. 
Obs.2: - Veja o exemplo de um log de consentimento no diretorio deste projeto.

## Links
- https://editor.swagger.io/
- https://sensedia.atlassian.net/wiki/spaces/OFD/pages/2550759427/Procedimento+Como+acessar+o+log+de+consentimento+no+AWS+SQS

